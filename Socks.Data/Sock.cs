﻿using System;

namespace Socks.Data
{
    public class Sock
    {
        private static Sock instance;

        

        public int Id { get; set; }

        public SockType Type { get; set; }

        public decimal Price { get; set; }

        public DateTime CreationDate { get; set; }

        private Sock()
        {
            Random rnd = new Random();
            this.CreationDate = DateTime.Now;
            this.Price = rnd.Next(0, 3);
        }

        public static Sock Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Sock();
                }

                return instance;
            }
        }
    }
}
