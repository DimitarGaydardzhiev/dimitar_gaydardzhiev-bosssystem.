﻿using Socks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Socks.Web.Models
{
    public class UserInformationViewModel
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public DateTime RegistrationDate { get; set; }

        public decimal Balance { get; set; }

        public static Expression<Func<ApplicationUser, UserInformationViewModel>> ViewModel
        {
            get
            {
                return u => new UserInformationViewModel()
                {
                    FullName = u.FullName,
                    Email = u.Email,
                    RegistrationDate = u.RegistrationDate,
                    Balance = u.Balance
                };
            }
        }
    }
}