﻿using Socks.Data;
using System;
using System.Linq.Expressions;

namespace Socks.Web.Models
{
    public class SockInformationViewModel
    {
        public decimal Price { get; set; }

        public static Expression<Func<Sock, SockInformationViewModel>> ViewModel
        {
            get
            {
                return s => new SockInformationViewModel()
                {
                    Price = s.Price
                };
            }
        }
    }
}