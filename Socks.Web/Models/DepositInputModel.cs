﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Socks.Web.Models
{
    public class DepositInputModel
    {
        [Required(ErrorMessage = "Deposit amount is required.")]
        public decimal Amount { get; set; }
    }
}