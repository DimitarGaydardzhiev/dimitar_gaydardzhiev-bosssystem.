﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Socks.Web.Startup))]
namespace Socks.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
