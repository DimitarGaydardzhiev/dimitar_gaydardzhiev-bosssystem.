﻿using Microsoft.AspNet.Identity;
using Socks.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Socks.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateDepositForm()
        {
            var model = new DepositInputModel();

            return View("CreateDepositForm");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DoDeposit(decimal amount)
        {
            string currentUserId = this.User.Identity.GetUserId();
            this.db.Users.Where(u => u.Id == currentUserId).Select(u => u).ToList()[0].Balance += amount;
            db.SaveChanges();

            return this.RedirectToAction("Index");
        }
    }
}