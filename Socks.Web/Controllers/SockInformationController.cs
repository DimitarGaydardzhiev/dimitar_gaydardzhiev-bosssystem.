﻿using Socks.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Socks.Web.Controllers
{
    public class SockInformationController : BaseController
    {
        public ActionResult GetSockPrice()
        {
            Random rnd = new Random();

            if (DateTime.Now.Subtract(sock.CreationDate) > TimeSpan.FromHours(1))
            {
                sock.Price = sock.Price * rnd.Next(0, 3);
            }

            return View(new SockInformationViewModel()
            {
                Price = sock.Price
            });
        }
    }
}