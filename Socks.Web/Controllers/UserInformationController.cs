﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Socks.Web.Models;
using System.Globalization;

namespace Socks.Web.Controllers
{
    public class UserInformationController : BaseController
    {
        public ActionResult GetUserInformation()
        {
            string currentUserId = this.User.Identity.GetUserId();
            var userInfo = this.db.Users.Where(u => u.Id == currentUserId).Select(UserInformationViewModel.ViewModel).ToList();

            return View(new UserInformationViewModel()
            {
                FullName = userInfo[0].FullName,
                Email = userInfo[0].Email,
                RegistrationDate = userInfo[0].RegistrationDate,
                Balance = userInfo[0].Balance
            });
        }
    }
}