﻿using Socks.Data;
using System.Web.Mvc;

namespace Socks.Web.Controllers
{
    public class BaseController : Controller
    {
        protected ApplicationDbContext db = new ApplicationDbContext();

        protected Sock sock = Sock.Instance;
    }
}